﻿using System;

namespace Lab1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("***Lab1 Program***");
            Program program = new Program();
            program.calculateBMI();
        }

        public void calculateBMI()
        {
            bool keepAsking = true;
            while (keepAsking)
            {
                Console.Write("\nEnter measure - [Standard (1) / Metric (2)] or enter 0 to exit: : ");
                int metric = int.Parse(Console.ReadLine());
                if (metric == 0)
                {
                    keepAsking = false;
                    return;
                }
                else if (!(metric == 1 || metric == 2))
                {
                    Console.WriteLine("Error: Invalid input!");
                    continue;
                }
                Console.Write(String.Format("\nEnter your height (in {0}), weight (in {1}) and sex (M or F) or enter X to exit: ", metric == 1 ? "inches" : "meters", metric == 1 ? "pounds" : "kgs"));
                string[] input = Console.ReadLine().Split();
                if (input[0].ToLower() == "x")
                {
                    keepAsking = false;
                    return;
                }
                if (input.Length < 3)
                {
                    Console.WriteLine("Error: Insufficient input!");
                    continue;
                }
                try
                {
                    if (!(input[2].ToLower() == "m" || input[2].ToLower() == "f"))
                    {
                        throw new FormatException("Sex is invalid!");
                    }

                    float height = Single.Parse(input[0]);
                    float weight = Single.Parse(input[1]);
                    string sex = input[2].ToLower();
                    float BMI = metric == 1 ? ((weight / height) / height) * 703 : weight / (height * height);
                    Console.WriteLine("BMI: " + BMI.ToString("0.0"));
                    Console.Write("Result: ");

                    if (BMI < 17.5) Console.WriteLine("Anorexia");
                    else if (BMI >= 35 && BMI < 40) Console.WriteLine("Severely obese");
                    else if (BMI >= 40 && BMI < 50) Console.WriteLine("Morbidly obese");
                    else if (BMI >= 50 && BMI <= 60) Console.WriteLine("Super obese");
                    else if (sex == "m")
                    {
                        if (BMI < 20.7) Console.WriteLine("Underweight");
                        else if (BMI < 26.4) Console.WriteLine("In normal range");
                        else if (BMI < 27.8) Console.WriteLine("Marginally overweight");
                        else if (BMI < 31.1) Console.WriteLine("Overweight");
                        else Console.WriteLine("Very overweight or obese");
                    }
                    else if (sex == "f")
                    {
                        if (BMI < 19.1) Console.WriteLine("Underweight");
                        else if (BMI < 25.8) Console.WriteLine("In normal range");
                        else if (BMI < 27.3) Console.WriteLine("Marginally overweight");
                        else if (BMI < 32.3) Console.WriteLine("Overweight");
                        else Console.WriteLine("Very overweight or obese");
                    }
                } 
                catch (FormatException)
                {
                    Console.WriteLine("Error: Invalid input!");
                    continue;
                }
                
                
            }
        }

    }
}
